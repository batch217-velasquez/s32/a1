let http = require("http");

let port = 4000;


let server = http.createServer((req, res) => {
	//HTTP METHOD - GET
	//Get method means that we will be retrieving or reading information

	if(req.url == "/" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});	
		res.end("Welcome to booking system");
	}

	if(req.url == "/profile" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to your profile");
	}

	if(req.url == "/courses" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Here's our courses available");
	}

	if(req.url == "/addcourse" && req.method == "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Add course to our resources");
	}

	if(req.url == "/updatecourse" && req.method == "PUT"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Update a course to our resources");
	}

	if(req.url == "/archivecourses" && req.method == "DELETE"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Archive courses to our resources");
	}



});

server.listen(port);

console.log(`Server is running at localhost:${port}.`);